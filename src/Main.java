import java.io.*;

/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/27/14
 * Time: 4:29 PM
 *
 * Driver and parser for project 6/7 file format
 */
public class Main {

    public static void main(String[] args){

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line_in, team1, team2;
        int score1, score2;
        String[] team1_split;
        String[] team2_split;
        UDGraph g0 = new UDGraph();
        UDGraph g1 = new UDGraph();
        DGraph g2 = new DGraph();
        DGraph g3 = new DGraph();

        try {
            while ((line_in = in.readLine()) != null) {
                if (line_in.length() > 0) {
                    team1 = line_in.substring(0, 37);
                    team2 = line_in.substring(37, line_in.length());

                    team1_split = team1.split("\\s\\s+");
                    team2_split = team2.split("\\s\\s+");

                    team1 = team1_split[0].trim();
                    score1 = Integer.parseInt(team1_split[1].trim());

                    team2 = team2_split[0].trim();
                    score2 = Integer.parseInt(team2_split[1].trim());
                }
                else
                    break;

                //-- Graph 1
                if (score1 != score2) {
                    g0.addEdge(team1, team2);
                }
                else {
                    g0.addVertex(team1);
                    g0.addVertex(team2);
                }

                //-- Graph 2
                g1.addEdge(team1, team2);

                //-- Graph 3
                if(score1 > score2)
                    g2.addEdge(team1, team2);
                else if(score2 > score1)
                    g2.addEdge(team2, team1);
                else {
                    g2.addVertex(team1);
                    g2.addVertex(team2);
                }

                //-- Graph 4
                if(score1 > score2)
                    g3.addEdge(team1, team2);
                else if(score2 > score1)
                    g3.addEdge(team2, team1);
                else {
                    g3.addEdge(team1, team2);
                    g3.addEdge(team2, team1);
                }
            }

            g0.DFS();
            System.out.println("G0: " + g0.printTAG());

            g1.DFS();
            System.out.println("G1: " + g1.printTAG());

            System.out.print("G2: ");
            g2.determineSCC();
            System.out.println();

            System.out.print("G3: ");
            g3.determineSCC();


            in.close();
        } catch (IOException ex) {
            System.out.println("Error reading input line.");
        }

    }
}
