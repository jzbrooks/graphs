import java.util.*;

/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/27/14
 * Time: 4:31 PM
 *
 * Undirected(UD) graph class
 */

public class UDGraph {
    private HashMap<String, LinkedList<String>> g_table;
    private int NUM_EDGES;
    private boolean[] seen;
    ArrayList<String> arryList;

    private HashMap<Integer, LinkedList<String>> SCC;
    private LinkedList<String> scc_set;

    int[] sizes;

    int time;

    public UDGraph()
    {
        g_table = new HashMap<String, LinkedList<String>>();
        SCC = new HashMap<Integer, LinkedList<String>>();
        sizes  = new int[1000];
        for (int i=0;i<1000;i++) {
            sizes[i] = 0;
        }
        NUM_EDGES = 0;
        time = 0;
    }

    /**
     * Size method
     * @return size of graph (number of vertices)
     */
    public int size()
    {
        return g_table.size();
    }

    /**
     * Private data getter method
     * @return number of edges in the graph
     */
    public int getNUM_EDGES()
    {
        return NUM_EDGES;
    }

    /**
     * Determines if the graph has a particular string vertex
     * @param v vertex to be found
     * @return true if vertex v is found in graph
     */
    private boolean hasVertex(String v)
    {
        return g_table.containsKey(v);
    }

    /**
     * Adds a vertex (String) to the graph
     * @param v vertex string
     */
    public void addVertex(String v)
    {
        if(!hasVertex(v)) g_table.put(v, new LinkedList<String>());
    }

    /**
     * Private helper method to determine if the graph has an edge
     * @param v1 first vertex of edge (String)
     * @param v2 second vertex of edge (String)
     * @return true if the graph has the edge v1---v2
     */
    private boolean hasEdge(String v1, String v2)
    {
        return hasVertex(v1) && g_table.get(v1).contains(v2);
    }

    /**
     * Adds an relationship between two strings in the graph
     * @param v1 first vertex of edge (String)
     * @param v2 second vertex of edge (String)
     */
    public void addEdge(String v1, String v2)
    {
        if(!hasEdge(v1, v2)) NUM_EDGES++;
        if(!hasVertex(v1)) addVertex(v1);
        if(!hasVertex(v2)) addVertex(v2);
        g_table.get(v1).add(v2);
        g_table.get(v2).add(v1);
    }

    /**
     * DFS public method
     * Begins DFS at first element added to graph
     */
    public void DFS()
    {
        arryList  = new ArrayList<String>();
        arryList.addAll(g_table.keySet());
        seen = new boolean[size()];
        for (int i=0; i<size(); i++) {
            seen[i] = false;
        }

        int counter = 0;

        for (int i=0; time<size(); i++)
        {
            if (!seen[i]) {
                if(i==0) scc_set = new LinkedList<String>();
                DFS(arryList.get(i));
                if (scc_set.size()==0) scc_set.add(arryList.get(i));
                SCC.put(counter++, scc_set);
                scc_set = new LinkedList<String>();
            }
        }
    }

    /**
     * DFS Helper method
     * @param k String on which to execute DFS
     */
    private void DFS(String k)
    {
        scc_set.add(k);
        seen[arryList.indexOf(k)] = true;

        Iterator it = g_table.get(k).iterator();
        time++;
        while (it.hasNext()) {
            String str = (String) it.next();

            if(hasEdge(k,str) && !seen[arryList.indexOf(str)])
            {
                DFS(str);
            }
        }
    }

    /**
     * Prints strongly connected components
     */
    public void printSCC()
    {
        for (int i=0; i<SCC.size(); i++) {
            Iterator itr = SCC.get(i).iterator();
            System.out.print(i + ": ");
            while (itr.hasNext())
            {
                System.out.print(itr.next() + " ");
            }
            System.out.print("\n");
        }
    }

    /**
     * Getter for number of strongly connected components
     * @return number of strongly connected components
     */
    public int num_scc()
    {
        return SCC.size();
    }

    /**
     * Generates tag for multiset format.
     * form: "number_of_scc {size*multiplicity,size*multiplicity,...}
     * @return multiset string representation for strongly connected components
     */
    public String printTAG()
    {

        String s = "";

        s += num_scc() + " ";
        s += "{";

        for (int i=0; i<SCC.size(); i++) {
            LinkedList<String> set = SCC.get(i);
            sizes[set.size()]++;
        }

        for (int i=0; i<1000; i++) {
            if(sizes[i]>0)
            {
                s += i;
                s += "*";
                s += sizes[i];
                s += ",";
            }
        }
        String retString = s.substring(0,s.length()-1);
        retString += "}";
        return retString;
    }

    /**
     * Overrides string method for a graph
     * Prints nodes in the order added and their respective neighbors
     * @return String representation of a graph
     */
    @Override
    public String toString()
    {
        String s = "";
        for (Map.Entry<String, LinkedList<String>> v : g_table.entrySet()) {
            s += v.getKey() + ": ";
            for (Object o : v.getValue()) {
                s += o + " ";
            }
            s += "\n";
        }
        return s;
    }
}
