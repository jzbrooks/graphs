import java.util.*;
/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 4/4/14
 * Time: 1:22 PM
 *
 * Directed(D) Graph class
 */
public class DGraph {
    private HashMap<String, LinkedList<String>> g_table;
    private int NUM_EDGES;
    private boolean[] seen;
    ArrayList<String> arryList;
    ArrayList<String> new_arryList;
    DGraph reverse;

    private HashMap<Integer, LinkedList<String>> SCC;
    private LinkedList<String> scc_set;

    private HashMap<Integer, String> finishes;

    int[] sizes;

    int time;

    public DGraph()
    {
        g_table = new HashMap<String, LinkedList<String>>();
        SCC = new HashMap<Integer, LinkedList<String>>();
        sizes  = new int[1000];
        for (int i=0;i<1000;i++) {
            sizes[i] = 0;
        }
        NUM_EDGES = 0;
        time = 0;
    }

    /**
     * Size method
     * @return size of graph (number of vertices)
     */
    public int size()
    {
        return g_table.size();
    }

    /**
     * Private data getter method
     * @return number of edges in the graph
     */
    public int getNUM_EDGES()
    {
        return NUM_EDGES;
    }

    /**
     * Determines if the graph has a particular string vertex
     * @param v vertex to be found
     * @return true if vertex v is found in graph
     */
    private boolean hasVertex(String v)
    {
        return g_table.containsKey(v);
    }

    /**
     * Adds a vertex (String) to the graph
     * @param v vertex string
     */
    public void addVertex(String v)
    {
        if(!hasVertex(v)) g_table.put(v, new LinkedList<String>());
    }

    /**
     * Private helper method to determine if the graph has an edge
     * @param v1 first vertex of edge (String)
     * @param v2 second vertex of edge (String)
     * @return true if the graph has the edge v1---v2
     */
    private boolean hasEdge(String v1, String v2)
    {
        return hasVertex(v1) && g_table.get(v1).contains(v2);
    }

    /**
     * Adds an relationship from v1 to v2 in the graph
     * @param v1 first vertex of edge (String)
     * @param v2 second vertex of edge (String)
     */
    public void addEdge(String v1, String v2)
    {
        if(!hasEdge(v1, v2)) NUM_EDGES++;
        if(!hasVertex(v1)) addVertex(v1);
        if(!hasVertex(v2)) addVertex(v2);
        g_table.get(v1).add(v2);
    }

    /**
     * Number of strongly connected components
     * @return private int NUM_EDGES
     */
    public int num_scc() {return SCC.size();}

    /**
     * Reverse vertices of the graph
     * @return graph with reversed vertices
     */
    public DGraph reverse()
    {
        Object[] keys = g_table.keySet().toArray();
        DGraph ret = new DGraph();
        for (int i=0; i<size(); i++) {
            Iterator iterator = g_table.get(keys[i]).iterator();
            while(iterator.hasNext())
            {
                ret.addEdge((String) iterator.next(), (String) keys[i]);
            }
        }
        return ret;
    }

    /**
     * DFS public method starting with item first
     * added to the graph.
     */
    public void DFS()
    {
        finishes = new HashMap<Integer, String>();
        arryList = new ArrayList<String>(g_table.keySet());
        seen = new boolean[size()];
        for (int i=0; i<size(); i++)
            seen[i] = false;
        for (int j=0; j<size(); j++) {
            if(!seen[j])
                DFS(arryList.get(j));
        }
    }

    /**
     * DFS Helper method
     * @param k String on which to execute DFS
     */
    private void DFS(String k)
    {
        int n = arryList.indexOf(k);
        seen[n] = true;
        time++;
        Iterator it = g_table.get(k).iterator();
        while (it.hasNext()) {
            String s = (String) it.next();
            if (hasEdge(k, s) && !seen[arryList.indexOf(s)]) {
                DFS(s);
            }
        }
        finishes.put(++time,k);
    }

    /**
     * Helper method for Kosajaru's Alg
     * @param list ordering of vertices by finish times
     */
    private void determineSCC(ArrayList<String> list)
    {
        seen = new boolean[list.size()];
        for (int i=0; i<size(); i++)
            seen[i] = false;

        int counter = 0;

        if (list!=null) {
            for (String s : list) {
                if (!seen[list.indexOf(s)]) {
                    if (list.indexOf(s) == 0) scc_set = new LinkedList<String>();
                    determineSCC(s, list);
                    if (scc_set.size() == 0) scc_set.add(s);
                    SCC.put(counter++, scc_set);
                    scc_set = new LinkedList<String>();
                }
            }
        }
    }

    /**
     * Helper method for Kosajaru's Alg.
     * @param k current dfs string
     * @param list ordering of vertices by finish times
     */
    private void determineSCC(String k, ArrayList<String> list)
    {
        scc_set.add(k);
        int n = list.indexOf(k);
        seen[n] = true;
        if (g_table.get(k)!=null) {
            Iterator it = g_table.get(k).iterator();
            while (it.hasNext()) {
                String s = (String) it.next();
                if (hasEdge(k, s) && !seen[list.indexOf(s)]) {
                    determineSCC(s, list);
                }
            }
        }
    }

    /**
     * Prints tag's determined by Kosajaru's Alg.
     */
    public void determineSCC()
    {
        new_arryList = new ArrayList<String>();
        this.DFS();
        SortedSet<Integer> keys = new TreeSet<Integer>(finishes.keySet());
        Object[] ints = keys.toArray();
        for (int i=ints.length-1; i>=0; i--) {
            new_arryList.add(finishes.get(ints[i]));
        }
        reverse = this.reverse();
        reverse.determineSCC(new_arryList);
        System.out.print(reverse.printTAG());
    }

    /**
     * Generates tag for multiset format.
     * form: "number_of_scc {size*multiplicity,size*multiplicity,...}
     * @return multiset string representation for strongly connected components
     */
    public String printTAG()
    {
        String s = "";

        s += num_scc() + " ";
        s += "{";

        for (int i=0; i<SCC.size(); i++) {
            LinkedList<String> set = SCC.get(i);
            sizes[set.size()]++;
        }

        for (int i=0; i<1000; i++) {
            if(sizes[i]>0)
            {
                s += i;
                s += "*";
                s += sizes[i];
                s += ",";
            }
        }
        if (s.length()>3) {
            String retString = s.substring(0, s.length() - 1);
            retString += "}";
            return retString;
        }
        return s + "}";
    }


    /**
     * Overrides string method for a graph
     * Prints nodes in the order added and their respective neighbors
     * @return String representation of a graph
     */
    @Override
    public String toString()
    {
        String s = "";
        for (Map.Entry<String, LinkedList<String>> v : g_table.entrySet()) {
            s += v.getKey() + ": ";
            for (Object o : v.getValue()) {
                s += o + " ";
            }
            s += "\n";
        }
        return s;
    }
}
